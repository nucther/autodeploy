#!/usr/bin/env sh
CF_Api="https://api.cloudflare.com/client/v4"


auto_cf_add(){
    fulldomain=$1
    type=$2
    value=$3

    _info "Add new subdomain $fulldomain"
        
    CF_Key="${CF_Key:-$(_read_final SAVED_CF_Key)}"
    CF_Email="${CF_Email:-$(_read_final SAVED_CF_Email)}"

    if [ -z "$CF_Key" ] || [ -z "$CF_Email" ]; then
        CF_Key=""
        CF_Email=""
        _err "You didn't specify a cloudflare api key and email yet."
        _err "Please create the key and try again."
        return 1
    fi

    if ! _contains "$CF_Email" "@"; then
        _err "It seems that the CF_Email=$CF_Email is not a valid email address."
        _err "Please check and retry."
        return 1
    fi

    if ! _get_root "$fulldomain"; then
        _err "invalid domain"
        return 1
    fi

    if ! printf "%s" "$response" | grep \"success\":true >/dev/null; then
        _err "Error"
        return 1
    fi    

    _info $_domain_id

    case "$type" in 
        ip)
            _cf_rest GET "zones/${_domain_id}/dns_records?type=A&name=$fulldomain"

            if ! printf "%s" "$response" | grep \"success\":true >/dev/null; then
                _err "Error"
                return 1
            fi
            count=$(printf "%s\n" "$response" | _egrep_o "\"count\":[^,]*" | cut -d : -f 2)
            
            if [ "$count" = "1" ]; then
                _info "Domain exists"
                return 0
            else 
                if _cf_rest POST "zones/$_domain_id/dns_records" "{\"type\":\"A\",\"name\":\"$fulldomain\",\"content\":\"$value\",\"ttl\":1}"; then
                    if printf -- "%s" "$response" | grep "$fulldomain" >/dev/null; then
                        _info "Added, OK"
                        return 0
                    else
                        _err "Add A record error."
                        return 1
                    fi
                fi
            fi
            _err "Add A record error."
            return 1
            ;;
        cname)
            _cf_rest GET "zones/${_domain_id}/dns_records?type=CNAME&name=$fulldomain"

            if ! printf "%s" "$response" | grep \"success\":true >/dev/null; then
                _err "Error"
                return 1
            fi
            count=$(printf "%s\n" "$response" | _egrep_o "\"count\":[^,]*" | cut -d : -f 2)
            
            if [ "$count" = "1" ]; then
                _info "Domain exists"
                return 0
            else 
                if _cf_rest POST "zones/$_domain_id/dns_records" "{\"type\":\"CNAME\",\"name\":\"$fulldomain\",\"content\":\"$value\",\"ttl\":1}"; then
                    if printf -- "%s" "$response" | grep "$fulldomain" >/dev/null; then
                        _info "Added, OK"
                        return 0
                    else
                        _err "Add A record error."
                        return 1
                    fi
                fi
                _err "Add A record error."
                return 1
            fi
            ;;
    esac
}

auto_cf_rm(){
    fulldomain=$1
    type=$2
    value=$3

    CF_Key="${CF_Key:-$(_read_final SAVED_CF_Key)}"
    CF_Email="${CF_Email:-$(_read_final SAVED_CF_Email)}"

    if [ -z "$CF_Key" ] || [ -z "$CF_Email" ]; then
        CF_Key=""
        CF_Email=""
        _err "You didn't specify a cloudflare api key and email yet."
        _err "Please create the key and try again."
        return 1
    fi
    
    if ! _get_root "$fulldomain"; then
        _err "invalid domain"
        return 1
    fi

    case "$type" in 
        ip)
            ntype="A"
            ;;
        cname)
            ntype="CNAME"
            ;;
    esac

    _cf_rest GET "zones/${_domain_id}/dns_records?type=$ntype&name=$fulldomain&content=$value"

    if ! printf "%s" "$response" | grep \"success\":true >/dev/null; then
        _err "Error"
        return 1
    fi

    count=$(printf "%s\n" "$response" | _egrep_o "\"count\":[^,]*" | cut -d : -f 2)
    
    if [ "$count" = "0" ]; then
        _info "Don't need to remove."
    else
        record_id=$(printf "%s\n" "$response" | _egrep_o "\"id\":\"[^\"]*\"" | cut -d : -f 2 | tr -d \" | head -n 1)
        
        if [ -z "$record_id" ]; then
            _err "Can not get record id to remove."
            return 1
        fi
        if ! _cf_rest DELETE "zones/$_domain_id/dns_records/$record_id"; then
            _err "Delete record error."
            return 1
        fi
        return 0
    fi
}

_get_root(){
    domain=$1
    i=2
    p=1

    while true; do
        h=$(printf "%s" "$domain" | cut -d . -f $i-100)

        if [ -z "$h" ]; then 
            return 1
        fi

        if ! _cf_rest GET "zones?name=$h"; then 
            return 1
        fi

        if _contains "$response" "\"name\":\"$h\"" >/dev/null; then
        _domain_id=$(printf "%s\n" "$response" | _egrep_o "\[.\"id\":\"[^\"]*\"" | head -n 1 | cut -d : -f 2 | tr -d \")
        if [ "$_domain_id" ]; then
            _sub_domain=$(printf "%s" "$domain" | cut -d . -f 1-$p)
            _domain=$h
            return 0
        fi
        return 1
        fi
        p=$i
        i=$(_math "$i" + 1)
    done
    return 1
}

_cf_rest(){
    m=$1
    ep=$2
    data=$3

    export _H1="X-Auth-Email: $CF_Email"
    export _H2="X-Auth-Key: $CF_Key"
    export _H3="Content-Type: application/json"

    if [ "$m" != "GET" ]; then 
        response="$(_post "$data" "$CF_Api/$ep" "" "$m")"
    else 
        response="$(_get "$CF_Api/$ep")"
    fi

    if [ "$?" != "0" ]; then 
        _err "error $ep"
        return 1
    fi

    return 0
}