#!/bin/bash
############################################
#
# Gitlab Runner Autodeploy as subdomain
# alternative for kubernetes
# 
# Cpanel Version
# version: 1.1
#
# Tested on: debian, ubuntu
#
############################################
version="1.1"
updated="31 March 2020"

if type lsb_release >/dev/null 2>&1; then 
    os=$(lsb_release -a 2>&1 | grep 'Distributor ID'| sed -e 's/Distributor ID:[[:blank:]]//' | sed -e 's/\(.*\)/\L\1/')
    version=$(lsb_release -a 2>&1 | grep 'Codename'| sed -e 's/Codename:[[:blank:]]//' | sed -e 's/\(.*\)/\L\1/')
else 
    echo "Please install lsb-release"
    exit 1    
fi

if [ "$os" == 'debian' ] || [ "$os" == 'ubuntu' ]; then
    echo -e "Your OS is \e[0;31m$os\e[0m"
else
    echo -e "\e[0;32mThis bash only support Debian, Ubuntu, Linux Mint"
    echo -e "Your OS is \e[0;31m$os\e[0m"
    exit 1
fi

# if [[ -e "/etc/debian_version" ]]; then    
#     debian=$(lsb_release -a 2>&1 | grep 'Codename'| sed -e 's/Codename:[[:blank:]]//')
# else
#     echo "This bash only support Debian, Ubuntu, Linux Mint"
#     exit 0;
# fi

############ IS CURL READY? ##################
if type curl>/dev/null 2>&1; then 
    curl=1
fi

############# CHECK NGINX ########################
if type nginx >/dev/null 2>&1 ; then 
    nginxVersion=$(nginx -v 2>&1 | sed -n -e 's/.*nginx\/\(.*\)/\1/gp')

    if [ "$curl" == 1 ]; then
        nginxLatest=$(curl -s 'http://nginx.org/download/' | grep -oP 'href="nginx-\K[0-9]+\.[0-9]+\.[0-9]+' | sort -t. -rn -k1,1 -k2,2 -k3,3 | head -1)
    else 
        nginxLatest=$(wget -q -O - 'http://nginx.org/download/' |grep -oP 'href="nginx-\K[0-9]+\.[0-9]+\.[0-9]+' | sort -t. -rn -k1,1 -k2,2 -k3,3 | head -1)
    fi
fi

if type apachectl >/dev/null 2>&1 ; then
    apacheVersion=$(apachectl -v 2>&1 | sed -n -e 's/.*Apache\/\(.*\) .*/\1/gp')
fi

####### SET PATH ###################
if [ -z "$D_PATH" ]; then 
    if type autodeploy >/dev/null 2>&1; then 
        adBinPath=$(type autodeploy | awk '{ print $3 }')
        export D_PATH=$(dirname $(ls -al $adBinPath | awk '{ print $11 }'))        
    else 
        export D_PATH=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
    fi        
fi
export LE_WORKING_DIR="$D_PATH"

export config_path="$D_PATH/account.conf"

# Defined Color
export COLOR_NC='\e[0m' # No Color
export COLOR_WHITE='\e[1;37m'
export COLOR_BLACK='\e[0;30m'
export COLOR_BLUE='\e[0;34m'
export COLOR_LIGHT_BLUE='\e[1;34m'
export COLOR_GREEN='\e[0;32m'
export COLOR_LIGHT_GREEN='\e[1;32m'
export COLOR_CYAN='\e[0;36m'
export COLOR_LIGHT_CYAN='\e[1;36m'
export COLOR_RED='\e[0;31m'
export COLOR_LIGHT_RED='\e[1;31m'
export COLOR_PURPLE='\e[0;35m'
export COLOR_LIGHT_PURPLE='\e[1;35m'
export COLOR_BROWN='\e[0;33m'
export COLOR_YELLOW='\e[1;33m'
export COLOR_GRAY='\e[0;30m'
export COLOR_LIGHT_GRAY='\e[0;37m'

_info(){
    echo -e "[$(date)]${COLOR_LIGHT_BLUE} $1 ${COLOR_NC}"
}
_err(){
    echo -e "[$(date)]${COLOR_LIGHT_RED} $1 ${COLOR_NC}"
}
_ok(){
    echo -e "[$(date)]${COLOR_LIGHT_GREEN} $1 ${COLOR_NC}"
}


################# CONFIGURATION #########################
_read_conf() {
  _r_c_f="$1"
  _sdkey="$2"  
  if [ -f "$_r_c_f" ]; then
    (
      eval "$(grep "^$_sdkey *=" "$_r_c_f")"
      eval "printf \"%s\" \"\$$_sdkey\""
    )
  else
    _err "config file is empty, can not read $_sdkey"
  fi
}

_save_conf(){
    _r_c_f="$1"
    _sdkey="$2"
    _sdval="$3"

    if [ -f "$_r_c_f" ]; then 
        if grep "^$_sdkey *=" "$_r_c_f">/dev/null 2>&1; then             
            sed -i "s/$_sdkey *=\(.*\)/$_sdkey=$_sdval/g" "$_r_c_f"
        else
            echo "$_sdkey=$_sdval" >> "$_r_c_f"
        fi
    else 
        echo "$_sdkey=$_sdval" >> "$_r_c_f"
    fi

}

# read final config
_read_final(){
    _read_conf $config_path "$1"
}

_save_final(){
    _save_conf $config_path "$1" "$2"
}
################### ENG CONFIG ############################

# exit

##############################  START NGINX #################################
_install_nginx(){    
    
    if [ "$nginxVersion" != "$nginxLatest" ]; then 
        _info "$text_info Your current nginx version $nginxVersion and current latest  version $nginxLatest."
        read -p "Do you want to update nginx? [Y/n]" -e -i "y" updateNginx
    fi

    read -p "Enctyption for DHPARAMS [ 2048 ]" -e -i "2048" dhparams

    if [ -z "$nginxVersion" ] || [ "$updateNginx" == "y" ] || [ "$updateNginx" == "Y" ]; then
        _info "Begin install Nginx"

        #if [ ! -e "/etc/apt/sources.list.d/nginx.list" ]; then
            echo "Add new source list for nginx"
            sudo echo "deb http://nginx.org/packages/mainline/$os/ $version nginx" > /etc/apt/sources.list.d/nginx.list
            sudo echo "deb-src http://nginx.org/packages/mainline/$os/ $version nginx" >> /etc/apt/sources.list.d/nginx.list
            wget -O - https://nginx.org/keys/nginx_signing.key | sudo apt-key add -
        #fi

        sudo apt update -y && apt install nginx -y

        sudo adduser --system --no-create-home --disabled-login --disabled-password --group nginx
        sudo usermod -g www-data nginx

        if [ ! -d "/etc/nginx/domain" ]; then 
            mkdir "/etc/nginx/domain"
        fi

        _config_nginx
    fi

    if [ ! -z "$nginxVersion" ]; then 
        _info "Nginx Already installed with version ${COLOR_LIGHT_RED}$nginxVersion ${COLOR_NC}"

        _config_nginx
    fi
}

_config_nginx(){
    _info "Begin set configure and set security"
    sudo apt install -y openssl

    if [ ! -e "/etc/nginx/dhparam.pem" ]; then     
        openssl dhparam -out /etc/nginx/dhparam.pem $dhparams
    fi
    mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.default

    nginx_conf=$(cat << EOF

user nginx;
worker_processes  5;
pcre_jit on;

pid         /var/run/nginx.pid;
error_log   /var/log/nginx/error.log warn;

include     /etc/nginx/modules/*.conf;

events {
	worker_connections 1024;
}

http {

	include /etc/nginx/mime.types;
	default_type application/octet-stream;	
	
	client_max_body_size 20m;	
	sendfile on;
	tcp_nodelay on;
	
	ssl_protocols TLSv1.2 TLSv1.3;# Requires nginx >= 1.13.0 else use TLSv1.2
	ssl_prefer_server_ciphers on; 
	ssl_dhparam /etc/nginx/dhparam.pem; # openssl dhparam -out /etc/nginx/dhparam.pem 4096
	ssl_ciphers ECDHE-RSA-AES256-GCM-SHA512:DHE-RSA-AES256-GCM-SHA512:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384;
	ssl_ecdh_curve secp384r1; # Requires nginx >= 1.1.0
	ssl_session_timeout  10m;
	ssl_session_cache shared:SSL:10m;
	ssl_session_tickets off; # Requires nginx >= 1.5.9
	ssl_stapling on; # Requires nginx >= 1.3.7
	ssl_stapling_verify on; # Requires nginx => 1.3.7

    #resolver 208.67.222.222 208.67.220.220;
	#resolver \$DNS-IP-1 \$DNS-IP-2 valid=300s;
	resolver_timeout 5s;

    #security
    server_tokens off;
	add_header Strict-Transport-Security "max-age=63072000; includeSubDomains; preload";
	add_header X-Frame-Options DENY;
	add_header X-Content-Type-Options nosniff;
	add_header X-XSS-Protection "1; mode=block";    

    # Slow DDOS mitigration from https://www.owasp.org/index.php/SCG_WS_nginx
    client_body_timeout   10;
    client_header_timeout 10;
    keepalive_timeout     55;
    send_timeout          10;   

	#gzip on;
	gzip_vary on;
	#gzip_static on;
	log_format main '\$remote_addr - \$remote_user [\$time_local] "\$request" '
			'\$status \$body_bytes_sent "\$http_referer" '
			'"\$http_user_agent" "\$http_x_forwarded_for"';
	access_log /var/log/nginx/access.log main;
	include /etc/nginx/conf.d/*.conf;
    include /etc/nginx/domain/*.conf;
}

EOF
)

    echo "$nginx_conf" > /etc/nginx/nginx.conf

    _generate_nginx_conf "localhost" "host" "/usr/share/nginx/html"
    _check_nginx_conf
}

_check_nginx_conf(){
    verify=$(nginx -t 2>&1 | grep "syntax" | sed -n 's/.*syntax is \(.*\)/\1/gp')
    echo $verify
    if [ "$verify" == "ok" ]; then 
        systemctl reload nginx
    else 
        _err "Please check nginx config."
        exit 0
    fi
}

# Usage
# _generate_nginx_conf domain [host/proxy] [directory/ host target]
# 
_generate_nginx_conf(){
    _domain=$1
    _conf_type=$2 # host/proxy
    _target=$3 # directory/ proxytarget

    isWildcard=$(_read_final "SSL_WILDCARD")
    if [ "$isWildcard" == 1 ]; then
        _cert="*.$(_read_final "MAIN_DOMAIN").pem"
        _key="*.$(_read_final "MAIN_DOMAIN").key"
    else 
        _cert="$_domain.pem"
        _key="$_domain.key"
    fi
    
    _conf_ssl_start=$(cat << EOF 
#default config for localhost
server {
    listen              443 ssl http2;
    server_name         $_domain;
    ssl_certificate     /etc/ssl/letsencrypt/$_cert;
    ssl_certificate_key /etc/ssl/letsencrypt/$_key;
    error_page  500 502 503 504     /50x.html;
    location = 50x.html {
        root        /usr/share/nginx/html;
    }
    
    #limit header request
    if (\$request_method !~ ^(GET|PUT|POST|DELETE)$){
        return 444;
    }    
EOF
)

    _conf_ssl_end="}"

    _conf_http=$(cat << EOF
server {
    listen          80;
    server_name     $_domain;
    return 301 https://\$host\$request_uri;}
EOF
)

    _conf_directory=$(cat << EOF

    location / {
        root $_target;
        index index.html index.htm app.html app.htm;
    }

EOF
)

    _conf_proxy=$(cat << EOF

    location / {
        proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_pass $_target;
    }

EOF
)

    echo "$_conf_http" > "/etc/nginx/domain/$_domain.conf"
    echo "$_conf_ssl_start" >> "/etc/nginx/domain/$_domain.conf"
    
    if [ "$_conf_type" == "proxy" ]; then 
        echo "$_conf_proxy" >> "/etc/nginx/domain/$_domain.conf"
    else
        echo "$_conf_directory" >> "/etc/nginx/domain/$_domain.conf"
    fi

    echo "$_conf_ssl_end" >> "/etc/nginx/domain/$_domain.conf"

}

########################### END NGINX #############################

######################### START APACHE ############################
_install_apache(){
    _info "Begin Installation Apache"

    # echo "  SSLEngine on" >> "/etc/apache2/sites-available/$_domain.conf"
    # echo "  SSLCipherSuite EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH" >> "/etc/apache2/sites-available/$_domain.conf"
    # echo "  SSLProtocol All -SSLv2 -SSLv3 -TLSv1 -TLSv1.1" >> "/etc/apache2/sites-available/$_domain.conf"
    # echo "  SSLHonorCipherOrder On" >> "/etc/apache2/sites-available/$_domain.conf"
    # echo "  Header always set Strict-Transport-Security \"max-age=63072000; includeSubDomains; preload\"" >> "/etc/apache2/sites-available/$_domain.conf"
    # echo "  Header always set X-Frame-Options DENY" >> "/etc/apache2/sites-available/$_domain.conf"
    # echo "  Header always set X-Content-Type-Options nosniff" >> "/etc/apache2/sites-available/$_domain.conf"
    # # Requires Apache >= 2.4
    # echo "  SSLCompression off" >> "/etc/apache2/sites-available/$_domain.conf"
    # echo "  SSLUseStapling on" >> "/etc/apache2/sites-available/$_domain.conf"
    # #echo "  SSLStaplingCache \"shmcb:logs/stapling-cache(150000)\"" >> "/etc/apache2/sites-available/$_domain.conf"
    # # Requires Apache >= 2.4.11
    # echo "  SSLSessionTickets Off" >> "/etc/apache2/sites-available/$_domain.conf"

}


_generate_apache_conf(){
    _domain=$1
    _type=$2
    _target=$3

    _info "Generate Apache Config"

    if [ -f "/etc/apache2/sites-available/$_domain.conf" ]; then 
        touch "/etc/apache2/sites-available/$_domain.conf"
    fi   

    isWildcard=$(_read_final "SSL_WILDCARD")
    if [ "$isWildcard" == 1 ]; then
        _cert="$(_read_final "MAIN_DOMAIN").pem"
        _key="$(_read_final "MAIN_DOMAIN").key"
    else 
        _cert="$_domain.pem"
        _key="$_domain.key"
    fi
    

    _conf_http=$(cat << EOF
<VirtualHost *:80>
    ServerName $_domain
    DocumentRoot $_path
    Redirect permanent / https://$_domain
</VirtualHost>"
EOF    
)

    _conf_ssl_start=$(cat << EOF
<VirtualHost *:443>
    ServerName $_domain
    SSLCertificateFile  /etc/ssl/letsencrypt/$_cert.cer
    SSLCertificateKeyFile   /etc/ssl/letsencrypt/$_key.key
EOF    
)

    _conf_ssl_end="</VirtualHost>"

    _conf_directory=$(cat << EOF
<Directory $_target>
    Require all granted
</Directory>
EOF    
)

    _conf_proxy=$(cat << EOF 
    ProxyPreserveHost On
    ProxyPass / $_target
    ProxyPassReverse / $_target
EOF    
)

    echo "$_conf_http" >> "/etc/apache2/sites-available/$_domain.conf"
    echo "$_conf_ssl_start" >> "/etc/apache2/sites-available/$_domain.conf"

    if [ "$_type" == "proxy" ]; then 
        echo "$_conf_proxy" >> "/etc/apache2/sites-available/$_domain.conf"
    else
        echo "$_conf_directory" >> "/etc/apache2/sites-available/$_domain.conf"
    fi

    echo "$_conf_ssl_end" >> "/etc/apache2/sites-available/$_domain.conf"

    ln -s "/etc/apache2/sites-available/$_domain.conf" "/etc/apache2/sites-enabled/$_domain.conf"    
}
######################## END APACHE ##############################

######################## Hardening Server ########################
_hardening(){
    _info "Try to improve server security"
    read -p "Enter your ssh port: " -e -i 8222 sshport
    if ! type sudo>/dev/null 2>&1; then 
        apt install sudo -y
    fi
    sudo sed -i "s/#Port .*/Port $sshport/g" "/etc/ssh/sshd_config"
    
    sudo apt update && sudo apt upgrade -y

    sudo apt install ufw -y

    sudo ufw allow http
    sudo ufw allow ssl
    sudo ufw allow "$sshport"

    sudo ufw enable
}
######################## End Hardening Server ####################

# Install and configure required software or application
install(){
    echo ""
    _info "Install Web server:"
    echo " 1) Apache"
    echo " 2) NginX"    
    read -p "Select an option [1-2]: " -e -i 2 WEBSERVER    
    case $WEBSERVER in 
        1) 
            _install_apache
            _save_final "WEBSERVER" "apache"
            ;;
        2) 
            _install_nginx
            _save_final "WEBSERVER" "nginx"
            ;;
    esac
}

install_app(){
    echo ""
    _info "Select application you want to install"
    echo -e "1) NodeJS"
    echo -e "2) Dot Net Core"
    echo -e "3) Gitlab Runner"
    echo -e "4) Docker"
    echo -e "5) Docker Compose"
    echo -e "99) Finished"
    read -p "Please enter your choice: " -e -i 99 _app_selected

    case $_app_selected in
        1)
            _info "Starting Install NodeJS"
            _install_nodejs

            _app_path=$(type node | awk '{ print $3 }')
            _save_final "app_node" "$_app_path"
            install_app
            ;;
        2)
            _info "Starting Install Dot Net Core"

            _save_final "app_dotnet" ""
            install_app
            ;;
        3)
            _info "Starting Install Gitlab Runner"
            _install_gitlabrunner
            # _save_final "app_runner" ""
            install_app
            ;;
        4)
            _info "Starting Install Docker"
            _install_docker
            # _save_final "app_docker" ""
            install_app
            ;;
        5)
            _info "Starting Install Docker Compose"
            _install_docker_compose
            install_app
            ;;
        99) exit 1;;
    esac
}

################# NodeJS Install ###############################
_install_nodejs(){
    _info "Install Node Js"

    if [ "$curl" == "1" ]; then 
        curl -sL https://deb.nodesource.com/setup_13.x | sudo -E bash -
    else 
        wget -O - https://deb.nodesource.com/setup_13.x | sudo -E bash -
    fi

    sudo apt-get install -y nodejs build-essential
}
################# End NodeJS Install ###########################

############### Gitlab Runner #########################
_install_gitlabrunner(){
    if [ "$curl" == "1" ]; then 
        curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
    else 
        wget -O - https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash -
    fi

    sudo cat > /etc/apt/preferences.d/pin-gitlab-runner.pref <<EOF
Explanation: Prefer GitLab provided packages over the Debian native ones
Package: gitlab-runner
Pin: origin packages.gitlab.com
Pin-Priority: 1001
EOF

    sudo apt update && sudo apt install -y gitlab-runner
}
############### END Gitlab Runner #########################

############## Docker ##########################
_install_docker(){
    os=$(lsb_release -is)

    case "$os" in 
        Ubuntu | ubuntu)
            sudo apt-get -y install apt-transport-https ca-certificates curl gnupg2 software-properties-common
            curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
            sudo echo "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" > "/etc/apt/sources.list.d/docker.list"
            sudo apt update -y && sudo apt install docker-ce -y
            ;;
        Debian | LinuxMint)
            sudo apt-get -y install apt-transport-https ca-certificates curl gnupg2 software-properties-common
            curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
            sudo echo "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" > "/etc/apt/sources.list.d/docker.list"
            sudo apt update -y && sudo apt install docker-ce -y
            ;;
    esac

    sudo groupadd docker

    if type gitlab-runner>/dev/null 2>&1; then 
        sudo usermod -aG docker gitlab-runner 
    fi

    if [ "$USER" != "root" ]; then 
        sudo usermod -aG docker $USER
    fi
}
############## END Docker ##########################

#### Docker Compose ##############
_install_docker_compose(){
    if [ "$curl" == "1" ]; then 
        sudo curl -L https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
    else 
        sudo wget -O /usr/local/bin/docker-compose https://github.com/docker/compose/releases/download/1.22.0/docker-compose-$(uname -s)-$(uname -m)
    fi

    sudo chmod +x /usr/local/bin/docker-compose
}
#### END Docker Compose ##############

# Generate Letsenctypt SSL use acme.sh
_generate_ssl(){
    _subdomain=$1
    
    MAIN_DOMAIN="$(_read_final "MAIN_DOMAIN")"
    DNS_TYPE="dns_$(_read_final "DNS_TYPE")"

    if [ "$_subdomain" == "*" ]; then 
        fulldomain="*.$MAIN_DOMAIN"
    else 
        fulldomain="$_subdomain.$MAIN_DOMAIN"
    fi

    _info "Check cert status for $MAIN_DOMAIN"

    if $LE_WORKING_DIR/acme.sh --list 2>&1 | grep "$fulldomain";
    then
        _ok "Certifcate already created waiting for renew."
    else
        echo "Create new cert $fulldomain"
        if [ ! -d "/etc/ssl/letsencrypt" ]; then 
            mkdir /etc/ssl/letsencrypt
        fi        

        if [ ! "$__test" == 1 ]; then 
            letsencrypt=$(sudo $LE_WORKING_DIR/acme.sh --issue -d $fulldomain --dns $DNS_TYPE --log-level 2 --log $LE_WORKING_DIR/acme.log --cert-file /etc/ssl/letsencrypt/$fulldomain.cer --key-file /etc/ssl/letsencrypt/$fulldomain.key --ca-file /etc/ssl/letsencrypt/$fulldomain.csr --fullchain-file /etc/ssl/letsencrypt/$fulldomain.pem)
        else 
            letsencrypt=$(sudo $LE_WORKING_DIR/acme.sh --issue -d $fulldomain --dns $DNS_TYPE --test --log-level 2 --log $LE_WORKING_DIR/acme.log --cert-file /etc/ssl/letsencrypt/$fulldomain.cer --key-file /etc/ssl/letsencrypt/$fulldomain.key --ca-file /etc/ssl/letsencrypt/$fulldomain.csr --fullchain-file /etc/ssl/letsencrypt/$fulldomain.pem)
        fi
    fi
}

_generate_backdomain(){
    _subdomain=$1

    
}


#### Systemd Config ####################
_generate_systemd_conf(){
    _app=$2
    _domain=$1    
    _port=$3

    _info "Generate systemd config for $_domain"
    _execPath="$(_read_final "app_$_app")"
case "$_app" in
    node)
        _execApp="app.js"
        ;;
    dotnet)
        _execApp="app.dll"
        ;;
esac

    _systemd_conf=$(cat << EOF
[Unit]  
Description=Service for $_app App for $_domain
After=network.target

[Service]
Environment=PORT=$_port $__env
WorkingDirectory=/home/$_domain
ExecStart=$_execPath /home/$_domain/$_execApp
Restart=on-failure

[Install]
WantedBy=multi-user.target
    
EOF
)
    sudo echo "$_systemd_conf" > "/etc/systemd/system/$_domain.service"
    sudo systemctl daemon-reload
    sudo systemctl enable "$_domain.service"
    sudo systemctl start "$_domain.service"
}
######### END Systemd Config ####################

#### Delete Systemd conf #####
_delete_systemd_conf(){
    _info "Delete systemd conf for $__domain"

    sudo systemctl stop "$__domain.service"
    sudo systemctl disable "$__domain.service"
    sudo rm -r "/etc/systemd/system/$__domain.service"
    sudo systemctl daemon-reload
}
#### Delete Systemd #####

#TODO: Create _generate_systemd_timer
#TODO: Create _delete_systemd_timer

#TODO: need to be fixed
#### Save Or Read Domain ############
_saveOrRead_domain(){
    _domain=$1
    _target=$2

    if [ ! -f "$D_PATH/record.txt" ]; then 
        sudo touch "$D_PATH/record.txt"
    fi

    path="$(grep "$_domain" "$D_PATH/record.txt" | sed 's/$__domain://g')"    
    
    if [ -z "$path" ] && [ "$path" != "" ]; then 
        echo "$path"
    else 
        sudo echo "$__domain:$_target" >> "$D_PATH/record.txt"
        echo "$_target"        
    fi
}
#### END Save Or Read Domain ############

#### Delete Domain record #######
_delete_domain_record(){
    _domain=$1
    sed -i 's/test.nurohman.com:.*/\d/g' "$D_PATH/record.txt"
}
#### Delete Domain record #######

#### Save or Read port #######
_saveOrRead_port(){
    _domain=$1

    if [ ! -f "$D_PATH/port.list" ]; then
        sudo touch "$D_PATH/port.list"
    fi

    lport=$(($(echo "$(tail -1 ${D_PATH}/port.list)" | sed -n -e 's/\(.*\):\(.*\)/\2/gp') + 1))
    if [ -z "$lport" ] || [ "$lport" == "1" ]; then 
        lport="4000"
    fi

    cport=$(grep "$_domain" "$D_PATH/port.list" | head -1 | sed -n -e 's/\(.*\):\(.*\)/\2/gp')

    nport=$(grep "unused" "$D_PATH/port.list" | head -1 | sed 's/unused://g')

    if [ ! -z "$cport" ]; then 
        echo "$cport"
    elif [ ! -z "$nport" ]; then 
        echo "$nport"
        sudo sed -i "s/unused:$nport/$_domain:$nport/g" "$D_PATH/port.list"
    else
        echo "$lport"
        sudo echo "$_domain:$lport" >> "$D_PATH/port.list"
    fi
}
#### Save or Read port #######

#### Delete Port #############
_delete_port(){
    _domain=$1

    sudo sed -i "s/$_domain/unused/g" "$D_PATH/port.list"
}
#### Delete Port #############

##### Copy & Delete ################
_copy_delete(){    
    _info "Copy And Delete files"

    if [ ! -d "$__target" ] || [ "$__file_only" == "0" ]; then
        sudo mkdir "$__target"
    fi

    if [ "$__clean" == "1" ]; then
        _info "Clean before copy files."
        sudo rm -r "$__target"
    fi

    _info "Copy files from $__source to $__target"
    sudo cp -fR "$__source" "$__target"

    if id www-data>/dev/null 2>&1; then
        sudo chown -R www-data:www-data "$__target"
    fi
}
##### End Copy & Delete ################

############# Version ##########################
if [ "$_version" = 1 ]; then 
    echo "Version: $version"
    echo "Updated: $updated"
    exit 1
fi

############# Install ##########################
_first_install(){

    if ! type sudo>/dev/null 2>&1; then 
        apt install sudo
    fi

    if [ -e "$D_PATH/account.conf" ]; then 
        _err "CAUTION PLEASE"
        read -p "Do you want to regenerate config file? [y/N]: " -e -i "n" _config_regenerate

        if [ "$_config_regenerate" == "y" ] || [ "$_config_regenerate" == "Y" ]; then 
            mv "$D_PATH/account.conf" "$D_PATH/account.conf.bak"
            _save_final "LE_WORKING_DIR" "$D_PATH"  
        fi
    else 
        touch "$D_PATH/account.conf"
    fi

    _default_domain=$(_read_final "MAIN_DOMAIN")
    
    _info "Please fill information bellow to decided what we will install."
    read -p "Please enter your main domain:" -e -i "$_default_domain" _ask_domain

    if [ "$_ask_domain" ]; then 
        _save_final "MAIN_DOMAIN" "$_ask_domain"
    fi

    _info "How we make subdomain?"
    echo -e "1) CNAME ( subdomain -> url address)"
    echo -e "2) A record ( subdomain -> server ip)"
    echo -e "3) wildcard subdomain ( *.main-domain.tld )"
    
    read -p  "Please enter your choice: " -e -i 2 _subdomain_type

    case $_subdomain_type in 
        1) 
            _save_final "SUB_TYPE" "cname"
            _default_cname=$(_read_final "CNAME_TARGET")
            read -p "please enter cname target: " -e -i "$_default_cname" _cname_target
            if [ ! -z "$_cname_target" ]; then 
                _save_final "CNAME_TARGET" "$_cname_target"
            fi
            ;;
        2)
            _save_final "SUB_TYPE" "ip"
            IP=$(ip addr | grep 'inet' | grep -v inet6 | grep -vE '127\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | grep -oE '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | head -1)
            read -p "Please enter server ip:" -e -i "$IP" _server_ip
            if echo "$IP" | grep -qE '^(10\.|172\.1[6789]\.|172\.2[0-9]\.|172\.3[01]\.|192\.168)'; then
                echo -e "$text_danger This server is behind NAT."
                read -p "What is your public IP:" -e _server_ip
            fi
            if [ ! -z "$_server_ip" ]; then 
                _save_final "SERVER_IP" "$_server_ip"
            fi
            ;;
        3)
            _save_final "SUB_TYPE" "wildcard"
            ;;
    esac

    _info "Please select dns server for domain above?"
    echo -e "1) CPANEL (WHM Reseller or Owner only)"
    echo -e "2) Cloudflare"
    read -p "Please enter your choice: " -e -i 2 _dns_server
    
    _info "Please enter required information bellow:"
    case $_dns_server in
        1)  
            _default_cp_url=$(_read_final "CP_URL")
            _default_cp_user=$(_read_final "CP_USER")
            _default_cp_key=$(_read_final "CP_KEY")
            read -p "enter WHM API URL: " -e -i "$_default_cp_url" _cpanel_url
            read -p "enter WHM API user: " -e -i "$_default_cp_user" _cpanel_user
            read -p "enter WHM API KEY: " -e -i "$_default_cp_key" _cpanel_key

            if [ ! -z $_cpanel_url ] && [ ! -z $_cpanel_user ] && [ ! -z $_cpanel_key ]; then 
                _save_final "CP_URL" "$_cpanel_url"
                _save_final "CP_USER" "$_cpanel_user"
                _save_final "CP_KEY" "$_cpanel_key"
                _save_final "DNS_TYPE" "whm"
            fi
            ;;
        2) 
            _default_cf_email=$(_read_final "SAVED_CF_Email")
            _default_cf_key=$(_read_final "SAVED_CF_Key")
            read -p "enter Cloudflare email: " -e -i "$_default_cf_email" _cf_email
            read -p "enter Cloudflare API key: " -e -i "$_default_cf_key" _cf_key

            if [ ! -z $_cf_email ] && [ ! -z $_cf_key ]; then 
                _save_final "SAVED_CF_Email" "$_cf_email"
                _save_final "SAVED_CF_Key" "$_cf_key"
                _save_final "DNS_TYPE" "cf"
            fi
            ;;
    esac

    _info "Please select how you generate SSL Certificate:"
    echo -e "1) Let's Encrypt (We will use https://acme.sh for generator)"
    echo -e "2) Buy your own SSL Certificate"
    read -p "Please enter your choice: " -e -i 1 _ssl_type

    case $_ssl_type in 
        1) 
            read -p "Do you want to use wildcard ssl (example *.domain.tld)?[Y/n]: " -e -i "y" _is_wildcard
            if [ "$_is_wildcard" == "Y" ] || [ "$_is_wildcard" == "y" ]; then 
                _save_final "SSL_WILDCARD" "1"
            fi
            _save_final "SSL_TYPE" "letsencrypt"
            ;;
        2) 
            _save_final "SSL_TYPE" "manual"
            _save_final "SSL_WILDCARD" "0"
            ;;
    esac
       

    _info "Instalation Starting"
    
    if [ ! -d "$D_PATH/dnsapi" ] && [ "$_test" == 1 ]; then 
        _info "Create Folder DNSAPI TEST"
        mkdir "$D_PATH/test-dnsapi"
        _dist_api="$D_PATH/test-dnsapi"
    elif [ ! -d "$D_PATH/dnsapi" ]; then 
        _info "Create Folder DNSAPI"
        mkdir "$D_PATH/dnsapi"
        _dist_api="$D_PATH/dnsapi"
    fi

    DNS_TYPE=$(_read_final "DNS_TYPE")

    case $DNS_TYPE in
        "cf")
            _info "Download Cloudflare bash API"
            wget -O "$_dist_api/dns_cf.sh" https://raw.githubusercontent.com/Neilpang/acme.sh/master/dnsapi/dns_cf.sh            
            wget -O "$_dist_api/auto_cf.sh" https://gitlab.com/nucther/autodeploy/raw/master/dnsapi/auto_cf.sh
        ;;

        "whm") 
            _info "Download CPANEL bash API"
        ;;
    esac

    SSL_TYPE=$(_read_final "SSL_TYPE")

    _info "Download Achme.sh"
    wget -O "$D_PATH/acme.sh" https://raw.githubusercontent.com/Neilpang/acme.sh/master/acme.sh        

    if [ "$SSL_TYPE" == "letsencrypt" ]; then         
        sed -i "s/DEFAULT_INSTALL_HOME=.*/DEFAULT_INSTALL_HOME=\"${D_PATH//\//\\\/}\"/g" "$D_PATH/acme.sh"
        sed -i 's/DEFAULT_ACCOUNT_KEY_LENGTH.*/DEFAULT_ACCOUNT_KEY_LENGTH=4096/g' "$D_PATH/acme.sh"
        sed -i 's/DEFAULT_DOMAIN_KEY_LENGTH.*/DEFAULT_DOMAIN_KEY_LENGTH=4096/g' "$D_PATH/acme.sh"
        chmod +x "$D_PATH/acme.sh"
        if [ ! -d "/etc/ssl/letsencrypt" ]; then 
            mkdir "/etc/ssl/letsencrypt"
        fi
    fi
    
    SSL_WILDCARD=$(_read_final "SSL_WILDCARD")

    if [ "$SSL_WILDCARD" == 1 ]; then 
        _generate_ssl "*"
    fi    

    install

    
    _ok "We detect that autodeploy is not installed as system"
    read -p "Do you want to install as system? [Y/n]" -e -i "y" _install_as_system
    if [ "$_install_as_system" == "y" ] || [ "$_install_as_system" == "Y" ]; then 
        echo "sudo $D_PATH/auto-deploy.sh \"\$@\"" > "$D_PATH/autodeploy.sh"
        sudo chmod +x "$D_PATH/autodeploy.sh"
        sudo ln -sf "$D_PATH/autodeploy.sh" "/usr/local/bin/autodeploy"
    fi    


    install_app
    
}

#### Remove Installation ##########################
_remove_installation(){
    rm -f /etc/apt/sources.list.d/nginx.list
    
    apt remove nginx -y && apt autoremove -y

    exit 0
}


_unknown(){
    _err "For more information please add -h or --help."
    exit 1
}

if [ ${#} == 0 ]; then 
    _unknown
fi


########### Generate Config ####################
_generate_conf(){
    domain=$1

    if type nginx>/dev/null 2>&1; then 
        _generate_nginx_conf
    fi

    if type apachectl>/dev/null 2>&1; then 
        _generate_apache_conf
    fi

    _err "Can't generate web server config. Delete subdomain from dns server"

}

#### Add Subdomain ######################
_add_subdomain(){

    SSL_TYPE=$(_read_final "SSL_TYPE")
    # if [ "$SSL_TYPE" == "letsencrypt" ]; then 
    . "$D_PATH/acme.sh" "-v"
    # fi

    DNS_TYPE=$(_read_final "DNS_TYPE")
    SUB_TYPE=$(_read_final "SUB_TYPE")
    dnsenv="auto_$DNS_TYPE"
    
    . "$D_PATH/dnsapi/$dnsenv.sh"

    case "$SUB_TYPE" in 
        ip)
            subdomain_target=$(_read_final "SERVER_IP")
            ;;
        cname)
            subdomain_target=$(_read_final "CNAME_TARGET")
            ;;
    esac

    subdomain="${dnsenv}_add"

    if $subdomain "$1" "$SUB_TYPE" "$subdomain_target"; then              
        _ok "Add new subdomain success"    
    else 
        _err "Failed add new subdomain"
        exit 1
    fi
}
########### END Add Subdomain ######################

####Delete Subdomain ###################
_delete_subdomain(){   

    DNS_TYPE=$(_read_final "DNS_TYPE")
    SUB_TYPE=$(_read_final "SUB_TYPE")
    dnsenv="auto_$DNS_TYPE"

    . "$D_PATH/acme.sh" "-v"    
    . "$D_PATH/dnsapi/$dnsenv.sh"

    case "$SUB_TYPE" in 
        ip)
            subdomain_target=$(_read_final "SERVER_IP")
            ;;
        cname)
            subdomain_target=$(_read_final "CNAME_TARGET")
            ;;
    esac

    subdomain="${dnsenv}_rm"

    if $subdomain "$__domain" "$SUB_TYPE" "$subdomain_target"; then              
        _ok "Delete subdomain success"    
    else 
        _err "Failed delete subdomain"
        exit 1
    fi

    WEBSERVER="$(_read_final "WEBSERVER")"
    case "$WEBSERVER" in 
        apache)
            _info "Delete apache configs"
            ;;
        nginx)
            _info "Delete nginx configs"
            sudo rm -r "/etc/nginx/domain/$__domain.conf"
            _check_nginx_conf
            ;;
    esac

    if [ -f "/etc/systemd/system/$__domain.service" ]; then 
        _delete_systemd_conf
    fi

    if [ "$__clean" == "1" ]; then 
        sudo rm -r "/home/$__domain"
    fi
}
########### END Delete Subdomain ###################

##### Help #####
_help(){ 
    echo "Usage autodeploy [options]"
    echo ""
    echo "[options]"
    echo ""
    echo "-d    --create-subdomain  Add domain"
    echo "-D    --delete-subdomain  Delete domain"
    echo "      --host-type         Set subdomain type [ proxy / host ]"
    echo "      --dest              Proxy destination"
    echo "      --test              Test generate certificate"
    echo ""
    echo "-s    --source            Source files or folder"
    echo "-t    --target            Target files or folder"
    echo "-c    --clean             Remove target before copy"
    echo "      --copy-only         Copy files without create subdomain"
    echo "      --file-only         Copy files without folder"
    echo ""
    echo "-e    --environment       Add other environment for systemd config"
    echo ""
    echo "-i                        Install Application"
    echo "      --install           Configure and install required application."
    echo "      --install-app       Install application only"
    echo "-u                        Update bash"
    echo "-v                        Version info"

    # exit 0
    return 0
}

#### Default 
__domain=""
__target=""
__source="$(pwd)"
__cmd=""
__subdir="0"
__dont_copy="0"
__file_only="0"

while [ -n "$1" ]; do    
    case "${1}" in
        --help | -h)
            __cmd="help"
            ;;
        --install)
            _first_install
            ;;
        --install-app)
            install_app
            ;;
        --source | -s)
            __source="${2}"
            ;;
        --target | -t)
            __target="${2}"
            ;;
        --clean | -c)
            __clean=1
            ;;
        --copy-only)
            __subdir="1"
            ;;
        --file-only)
            __file_only="1"
            ;;
        --test)
            __test="1"
            ;;
        --e | --enviromnent)
            __env=${$2/\"/}
            ;;
        --host-type)
            if [ -z "${2}" ] [ [ "${2}" != "host" ] || [ "${2}" != "proxy" ]]; then
                _err "usage --host-type [host/proxy]"
            fi
            __host_type="${2}"
            ;;
        --dest)
            if [ -z "${2}" ]; then
                _err "usage --host-type proxy --dest [destination host and port]"
            fi
            __destination="${2}"
            ;;
        -d | --create-subdomain)
            if [ -z "${2}" ]; then
                _err "subdomain is not valid."
                exit 0
            fi
            __cmd="add"
            __domain="${2}"
            ;;
        -D | --delete-subdomain)            
            __cmd="del"
            __domain="${2}"            
            ;;
    esac
    shift
done

if [ -z "$__cmd" ]; then
    _unknown
fi

#### start command ############
case "$__cmd" in 
    add)
        if [ "$__subdir" == "0" ]; then 
            _info "create new subdomain $__domain"
            _add_subdomain $__domain

            SSL_WILDCARD="$(_read_final "SSL_WILDCARD")"
            if [ "$SSL_WILDCARD" != "1" ]; then
                _generate_ssl $__domain
            fi
            
            __target="/home/$__domain/$__target"
            __target="$(_saveOrRead_domain $__domain $__target)"
            
            
            _info $__source
            _info $__target

            if [ "$__dont_copy" == "0" ]; then 
                _copy_delete        
            fi

           ls "$__source"

            #auto detect deploy type nodejs
            if [ -f "$__source/app.js" ]; then 
                _host_conf_type="proxy"
                _app_type="node"
                _ok "Node? yes"
            else 
                _err "Node? no"
            fi

            #auto detect deploy type dotnet
            if [ -f "$__source/app.dll" ]; then 
                _host_conf_type="proxy"
                _app_type="dotnet"
                _ok "DotNet? yes"
            else 
                _err "DotNet? no"
            fi

            #auto detect deploy type docker
            if [ -f "$__source/docker-compose.yml" ] || [ -f "$__source/Dockerfile" ]; then 
                _host_conf_type="proxy"
                _app_type="docker"
                _ok "Docker? yes"
            else 
                _err "Docker? no"
            fi

            if [ ! -z "$__host_type" ]; then 
                _host_conf_type="$__host_type"
            fi
            
            if [ "$_host_conf_type" == "proxy" ]; then 
                if [ ! -z "$__host_type" ]; then 
                    if [ -z "$__destination" ]; then 
                        _err "usage --host-type proxy --dest [destination host and port]"
                        exit 1
                    fi
                    __target="$__destination"
                else 
                    _host_port="$(_saveOrRead_port $__domain)"
                    _info "Set local port: $_host_port"                
                    __target="http://localhost:$_host_port"

                    _generate_systemd_conf $__domain $_app_type $_host_port
                fi
            fi

            _info "Host Type: $_host_conf_type"
        
            WEBSERVER="$(_read_final "WEBSERVER")"
            case "$WEBSERVER" in 
                apache)
                    _generate_apache_conf "$__domain" "$_host_conf_type" "$__target"
                    # _check_apache_conf
                    ;;
                nginx)
                    _generate_nginx_conf "$__domain" "$_host_conf_type" "$__target"
                    _check_nginx_conf
                    ;;
            esac
        else 
            _info "Copy Files Only"
            if [ -z "$__domain" ]; then 
                _err "subdomain not valid. usage -d [subdomain] --copy-only"
            fi
            __target="/home/$__domain/$__target"
            __target="$(_saveOrRead_domain $__domain $__target)"

            if [ "$__dont_copy" == "0" ]; then 
                _copy_delete        
            fi
            sudo systemctl restart "$__domain.service"
        fi
		;;
    del)
        _info "delete subdomain $__domain"
        _delete_subdomain
		;;
    help)
        _help
		;;
esac